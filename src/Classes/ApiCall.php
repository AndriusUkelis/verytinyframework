<?php

namespace Wtf\Classes;

use Exception;
use Wtf\Exceptions\CurlException;

/**
 * Class ApiCall
 * @package Wtf\Classes
 */
class ApiCall {
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const METHOD_OPTIONS = 'OPTIONS';

    const TIMEOUT_LENGTH = 10;

    /**
     * @var float $delay Number of seconds of delay between requests (can be fractional)
     */
    private $delay = 0;

    /**
     * @var array $options cUrl options array
     */
    private $options = [];

    /**
     * @var bool $decodeGz If should use gzdecode when parsing response
     */
    private $decodeGz = false;

    /**
     * @var bool $forceDecodeJSON If force decode JSON when parsing response
     */
    private $forceDecodeJSON = false;

    /**
     * @var bool $decodeBase64 If should decode base64 info when parsing response
     */
    private $decodeBase64 = false;

    /**
     * @var bool $utf8encode If should decode UTF-8 when parsing response
     */
    private $utf8encode = false;

    /**
     * ApiCall constructor.
     * @param string $url
     */
    public function __construct(string $url = '') {
        $this->options = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => self::METHOD_GET,
            CURLOPT_HTTPHEADER => [],
            CURLOPT_POSTFIELDS => null,
            CURLOPT_TIMEOUT => self::TIMEOUT_LENGTH,
            CURLOPT_CONNECTTIMEOUT => self::TIMEOUT_LENGTH,
        ];
    }

    /**
     * @return float
     */
    public function getDelay(): float {
        return $this->delay;
    }

    /**
     * @param float $delay
     * @return $this
     */
    public function setDelay(float $delay): self {
        $this->delay = $delay;
        return $this;
    }

    /**
     * @param string|null $url
     * @return ApiCallResponse
     */
    public function execute(string $url = null): ApiCallResponse {
        if (!empty($url)) {
            $this->setUrl($url);
        }
        $curl = $this->setupCurl();
        $this->setDelayIfNeeded();
        $headers = [];
        // this function is called by curl for each header received
        curl_setopt($curl, CURLOPT_HEADERFUNCTION, function ($curl, $header) use (&$headers) {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) { // ignore invalid headers
                return $len;
            }

            $name = strtolower(trim($header[0]));
            $headers[$name] = trim($header[1]);

            return $len;
        });

        try {
            $content = curl_exec($curl);
        } catch (Exception $e) {
            return ApiCallResponse::make(null, null);
        }
        $curlError = curl_errno($curl);
        if ($curlError > 0) {
            return ApiCallResponse::make(null, null, $curlError);
        }

        $status = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $content = $this->parseContent($curl, $content, $headers);

        return ApiCallResponse::make($content, $status, null, $headers);
    }

    private function setDelayIfNeeded(): void {
        if (!empty($this->delay)) {
            $this->setApiDelay();
        }
    }

    private function setApiDelay(): void {
        usleep($this->delay * 1000000);
    }

    /**
     * @param $curl
     * @param string $content
     * @param array $responseHeaders
     * @return mixed
     */
    private function parseContent($curl, string $content, array $responseHeaders) {
        $contentType = strtolower(curl_getinfo($curl, CURLINFO_CONTENT_TYPE));
        $contentType = explode(';', $contentType, 2);
        $contentType = trim($contentType[0]);

        curl_close($curl);

        if (!empty($responseHeaders['content-encoding'])
            && strpos($responseHeaders['content-encoding'], 'gzip') !== false
        ) {
            $content = $this->setGzdecode($content);
        } else {
            $content = $this->setGzDecodeIfNeeded($content);
        }

        $content = $this->decodeBase64IfNeeded($content);

        if ($contentType === 'application/json' || $this->isForceDecodeJSON()) {
            if ($this->isUtf8Encode()) {
                $content = utf8_encode($content);
            }

            $content = json_decode($content, true, 512, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
        return $content;
    }

    /**
     * @param $content
     * @return string|false
     */
    private function setGzDecodeIfNeeded($content) {
        if ($this->isDecodeGz()) {
            return $this->setGzdecode($content);
        }
        return $content;
    }

    /**
     * @param $content
     * @return string|false
     */
    private function setGzDecode($content) {
        return gzdecode($content);
    }

    /**
     * @param $content
     * @return string|false
     */
    private function decodeBase64IfNeeded($content) {
        if ($this->shouldDecodeBase64()) {
            return base64_decode($content, true);
        }
        return $content;
    }

    /**
     * @return false|resource
     */
    private function setupCurl() {
        $curl = curl_init();
        curl_setopt_array($curl, $this->getOptions());
        return $curl;
    }

    /**
     * @return string
     */
    public function getUrl(): string {
        return $this->options[CURLOPT_URL];
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url): self {
        $this->options[CURLOPT_URL] = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string {
        return $this->options[CURLOPT_CUSTOMREQUEST];
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod(string $method): self {
        $this->options[CURLOPT_CUSTOMREQUEST] = $method;
        return $this;
    }

    /**
     * @param string $proxy
     * @return $this
     */
    public function setProxy(string $proxy): self {
        /**
         * @TODO: username/password
         */
        if (!empty($proxy) &&
            preg_match('/^(.*:\/\/)?([^:]*):?([0-9]+)?$/', $proxy, $matches)
        ) {
            switch ($matches[1]) {
                case "http://":
                case '':
                    $this->options[CURLOPT_PROXYTYPE] = CURLPROXY_HTTP;
                    break;
                case 'https://':
                    // no PROXYTYPE for https in PHP, set through address
                    $matches[2] = $matches[1].$matches[2];
                    break;
                case 'socks4://':
                    $this->options[CURLOPT_PROXYTYPE] = CURLPROXY_SOCKS4;
                    break;
                case 'socks4a://':
                    $this->options[CURLOPT_PROXYTYPE] = CURLPROXY_SOCKS4A;
                    break;
                case 'socks5://':
                    $this->options[CURLOPT_PROXYTYPE] = CURLPROXY_SOCKS5;
                    break;
                case 'socks5h://':
                    $this->options[CURLOPT_PROXYTYPE] = CURLPROXY_SOCKS5_HOSTNAME;
                    break;
                default:
                    // bad protocol
                    return $this;
            }

            $this->options[CURLOPT_PROXY] = $matches[2];
            $this->options[CURLOPT_PROXYPORT] = (count($matches) === 4 ? $matches[3] : '1080');
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPostPayload() {
        return $this->options[CURLOPT_POSTFIELDS];
    }

    /**
     * @param mixed $postPayload
     * @return $this
     */
    public function setPostPayload($postPayload): self {
        $this->options[CURLOPT_POSTFIELDS] = $postPayload;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array {
        $this->options[CURLOPT_HTTPHEADER] = array_values($this->options[CURLOPT_HTTPHEADER]);
        return $this->options;
    }

    /**
     * @param mixed $name
     * @param mixed $value
     * @return $this
     */
    public function addOption($name, $value): self {
        $this->options[$name] = $value;
        return $this;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function removeOption($name): self {
        unset($this->options[$name]);
        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array {
        return $this->options[CURLOPT_HTTPHEADER];
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addHeader(string $name, string $value): self {
        $this->options[CURLOPT_HTTPHEADER][$name] = $name.': '.$value;
        return $this;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function addHeaders(array $headers): self {
        $this->options[CURLOPT_HTTPHEADER] = array_merge($this->options[CURLOPT_HTTPHEADER], $headers);
        return $this;
    }

    /**
     * @return bool
     */
    public function isDecodeGz(): bool {
        return $this->decodeGz;
    }

    /**
     * @param bool $decodeGz
     * @return $this
     */
    public function setDecodeGz(bool $decodeGz): self {
        $this->decodeGz = $decodeGz;
        return $this;
    }

    /**
     * @return bool
     */
    public function isForceDecodeJSON(): bool {
        return $this->forceDecodeJSON;
    }

    /**
     * @param bool $forceDecodeJSON
     * @return $this
     */
    public function setForceDecodeJSON(bool $forceDecodeJSON): self {
        $this->forceDecodeJSON = $forceDecodeJSON;
        return $this;
    }

    /**
     * @param bool $utf8encode
     * @return $this
     */
    public function setUtf8Encode(bool $utf8encode): self {
        $this->utf8encode = $utf8encode;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUtf8Encode(): bool {
        return $this->utf8encode;
    }

    /**
     * @param int $timeout
     * @return $this|bool
     */
    public function setTimeout(int $timeout) {
        if ($timeout < 0) {
            return false;
        }
        $this->options[CURLOPT_TIMEOUT] = $timeout;
        $this->options[CURLOPT_CONNECTTIMEOUT] = $timeout;
        return $this;
    }

    /**
     * @param bool $decodeBase64
     * @return $this
     */
    public function setDecodeBase64(bool $decodeBase64): self {
        $this->decodeBase64 = $decodeBase64;
        return $this;
    }

    /**
     * @return bool
     */
    public function shouldDecodeBase64(): bool {
        return $this->decodeBase64;
    }

    public function __clone() {
        $newInstance = new ApiCall($this->getUrl());
        $newInstance->addHeaders($this->getHeaders());
        foreach ($this->options as $name => $option) {
            $newInstance->addOption($name, $option);
        }

        $newInstance->setDecodeGz($this->decodeGz);
        $newInstance->setDecodeBase64($this->decodeBase64);
        $newInstance->setForceDecodeJSON($this->forceDecodeJSON);
        $newInstance->setDelay($this->delay);

        return $newInstance;
    }

    /**
     * @return $this
     */
    public function clearHeaders(): self {
        $this->options[CURLOPT_HTTPHEADER] = [];
        return $this;
    }

    /**
     * @deprecated
     * @param string $url
     * @param bool $post
     * @param array $post_payload_array
     * @param array $set_options
     * @return array
     * @throws CurlException
     */
    public function call(
        string $url,
        bool $post = false,
        array $post_payload_array = [],
        array $set_options = []
    ): array {
        $options = [
            CURLOPT_URL => $url,
            CURLOPT_POST => $post,
            CURLOPT_RETURNTRANSFER => true,
        ];

        foreach ($set_options as $key => $value) {
            $options[$key] = $value;
        }

        if ($post) {
            $options[CURLOPT_POSTFIELDS] = json_encode($post_payload_array);
            $options[CURLOPT_HTTPHEADER] = ['Content-type: application/json'];
        }

        $curl = curl_init();
        curl_setopt_array($curl, $options);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $content = curl_exec($curl);
        if (curl_errno($curl) > 0) {
            throw new CurlException(curl_error($curl));
        }

        $status = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $content_type = strtolower(curl_getinfo($curl, CURLINFO_CONTENT_TYPE));
        $content_type = explode(';', $content_type, 2);
        $content_type = trim($content_type[0]);

        curl_close($curl);

        if ($content_type === 'application/json') {
            $content = json_decode(utf8_encode($content), true, 512, JSON_PARTIAL_OUTPUT_ON_ERROR);
        }

        return [
            'status' => $status,
            'content' => $content,
        ];
    }

}
