<?php

namespace Wtf\Classes\Db;

use BadMethodCallException;

/**
 * Class DB
 * @package Wtf\Classes
 * @method static array fetchDocuments(string $collectionTitle, array $find = [], int $limit = null, array $sort = null, array $findOptions = [])
 * @method static \MongoDB\Driver\Cursor fetchDocumentsCursor(string $collection, array $find = [], int $limit = null, array $options = [])
 * @method static ?array fetchDocument(string $collection, array $find = [], array $sort = null)
 * @method static mixed fetchValue(string $collection, string $key, array $find = [], array $options = [])
 * @method static void insertOne(string $collectionTitle, array $document, bool $upsert = false)
 * @method static void insertIgnoreMany(string $collection, array $documents)
 * @method static void insertMany(string $collection, array $documents, bool $debugTime = false)
 * @method static void upsertMany(string $collection, array $documents, array $filterKeys, bool $debugTime = false)
 * @method static void updateOne(string $collectionTitle, array $filter, array $update, bool $upsert = false)
 * @method static void upsertOne(string $collectionTitle, array $filter, array $update)
 * @method static void updateMany(string $collectionTitle, array $filter, array $update, bool $upsert = false)
 * @method static void delete(string $collectionTitle, array $find = [])
 * @method static void replaceOne(string $collectionTitle, array $filter, array $replacement, bool $upsert = true)
 * @method static array getDistinctValues(string $collectionTitle, string $key, array $filter = [])
 * @method static ?int count(string $collectionTitle, array $filter, array $options = [])
 * @method static \Traversable|\MongoDB\Driver\Cursor aggregate(string $collectionTitle, array $pipeline, array $options = [])
 * @method static void executeBulkWrite(\MongoDB\Driver\BulkWrite $bulk, string $collectionTitle)
 * @method static \MongoDB\Collection collection(string $name, array $options = [])
 */
class MongoDb {

    /** @var MongoDbDriver[] $connectionPool */
    private static $connectionPool = [];

    /**
     * @param string $connection
     * @return MongoDbDriver
     */
    public static function connection(string $connection = ''): MongoDbDriver {
        $connectionString = strtolower($connection);
        if (!isset(self::$connectionPool[$connectionString])) {
            self::$connectionPool[$connectionString] = ConnectionManager::connect($connectionString);
        }
        return self::$connectionPool[$connectionString];
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic($name, $arguments) {
        $driver = static::connection();
        if (method_exists($driver, $name)) {
            return call_user_func([$driver, $name], ...$arguments);
        }
        throw new BadMethodCallException('Function "'.$name.'" does not exist!');
    }
}
