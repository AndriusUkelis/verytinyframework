<?php

namespace Wtf\Classes\Db;

use Wtf\Abstracts\Db\DbDriver;

class MysqlDbDriver extends DbDriver {
    public static function connect() {
        if (self::$link !== null) {
            return self::$link;
        }

        $host = Env::get('DB_HOST', 'localhost');
        $database = Env::get('DB_DATABASE');
        $user = Env::get('DB_USERNAME');
        $password = Env::get('DB_PASSWORD', '');
        $port = Env::get('DB_PORT', 3306);

        self::$link = mysqli_connect($host, $user, $password, $database, $port);
        if (!self::$link) {
            die(mysqli_connect_error());
        }

        return self::$link;
    }

    public static function query(string $sql) {
        $link = self::connect();
        $result = mysqli_query($link, $sql);
        if (!$result) {
            die(mysqli_error($link));
        }

        return $result;
    }

    public static function fetchRows(string $sql): array {
        $result = self::query($sql);

        $rows = [];
        while($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }

        return $rows;
    }

    public static function fetchRow(string $sql): ?array {
        $result = self::query($sql);
        return mysqli_fetch_assoc($result);
    }

    public static function fetchValue(string $sql) {
        $row = self::fetchRow($sql);
        if (empty($row)) {
            return null;
        }

        return array_values($row)[0];
    }

    public static function insertIgnoreMany(string $table, array $rows) {
        return self::insertMany($table, $rows, true);
    }

    public static function insertMany(string $table, array $rows, bool $ignore = false) {
        if (count($rows) < 1) {
            return;
        }

        if ($ignore) {
            $sqlTpl = 'INSERT IGNORE INTO `%s` (%s) VALUES (%s)';
        } else {
            $sqlTpl = 'INSERT INTO `%s` (%s) VALUES (%s)';
        }

        $link = self::connect();
        $firstRow = array_values($rows)[0];
        $keys = array_keys($firstRow);
        $values = [];
        foreach ($rows as $row) {
            $rowValues = [];
            foreach ($row as $value) {
                $rowValues[] = mysqli_real_escape_string($link, $value);
            }

            $values[] = "'".implode("', '", $rowValues)."'";
        }

        $valuesImploded = implode('),(', $values);
        $keysImploded = '`'.implode('`, `', $keys).'`';
        $sql = sprintf($sqlTpl, $table, $keysImploded, $valuesImploded);

        return self::query($sql);
    }

    public static function insertOnDuplicateUpdate(string $table, array $rows) {
        if (count($rows) < 1) {
            return;
        }

        $sqlTpl = 'INSERT INTO `%s` (%s) VALUES (%s)
            ON DUPLICATE KEY UPDATE %s';

        $link = self::connect();
        $firstRow = array_values($rows)[0];
        $keys = array_keys($firstRow);
        $values = [];
        foreach ($rows as $row) {
            $rowValues = [];
            foreach ($row as $value) {
                $rowValues[] = mysqli_real_escape_string($link, $value);
            }

            $values[] = "'".implode("', '", $rowValues)."'";
        }

        $updateFields = [];
        foreach ($keys as $key) {
            $updateFields[] = sprintf('`%1$s` = VALUES(`%1$s`)', $key);
        }

        $valuesImploded = implode('),(', $values);
        $keysImploded = '`'.implode('`, `', $keys).'`';
        $updateFieldsImploded = implode(',', $updateFields);
        $sql = sprintf($sqlTpl, $table, $keysImploded, $valuesImploded, $updateFieldsImploded);

        return self::query($sql);
    }

    public static function escape(string $value): string {
        $link = self::connect();
        return mysqli_real_escape_string($link, $value);
    }

    /**
     * Alias of beginTransaction()
     */
    public static function startTransaction() {
        return self::beginTransaction();
    }

    public static function beginTransaction() {
        $link = self::connect();
        mysqli_begin_transaction($link);
    }

    public static function beginTransactionReadOnly() {
        $link = self::connect();
        mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_ONLY);
    }

    public static function beginTransactionReadWrite() {
        $link = self::connect();
        self::setAutoCommit(false);
        mysqli_begin_transaction($link, MYSQLI_TRANS_START_READ_WRITE);
    }

    public static function beginTransactionWithConsistentSnapshot() {
        $link = self::connect();
        mysqli_begin_transaction($link, MYSQLI_TRANS_START_WITH_CONSISTENT_SNAPSHOT);
    }

    public static function commit() {
        $link = self::connect();
        mysqli_commit($link);
    }

    public static function rollback() {
        $link = self::connect();
        mysqli_rollback($link);
    }

    public static function setAutoCommit(bool $mode) {
        $link = self::connect();
        mysqli_autocommit($link, $mode);
    }
}
