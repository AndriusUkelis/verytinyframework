<?php

namespace Wtf\Classes;

/**
 * Class ApiCallResponse
 * @package Wtf\Classes
 */
class ApiCallResponse {
    /** @var mixed $content */
    protected $content;

    /** @var int|null $status HTTP status */
    protected $status = null;

    /** @var int|null $curlErrorCode */
    protected $curlErrorCode = null;

    /** @var array Response headers */
    protected $headers = [];

    /**
     * ApiCallResponse constructor.
     * @param $content
     * @param int|null $status
     * @param int|null $curlErrorCode
     * @param array $headers
     */
    protected function __construct($content, int $status = null, int $curlErrorCode = null, array $headers = []) {
        $this->content = $content;
        $this->status = $status;
        $this->curlErrorCode = $curlErrorCode;
        $this->headers = $headers;
    }

    /**
     * @param $content
     * @param int|null $status
     * @param int|null $curlErrorCode
     * @param array $headers
     * @return ApiCallResponse
     */
    static public function make($content, int $status = null, int $curlErrorCode = null, array $headers = []): self {
        return new static($content, $status, $curlErrorCode, $headers);
    }

    /**
     * @return mixed
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int {
        return $this->status;
    }

    /**
     * @return int|null
     */
    public function getCurlErrorCode(): ?int {
        return $this->curlErrorCode;
    }

    public function getHeaders(): array {
        return $this->headers;
    }

    public function getHeader(string $name): ?string {
        if (isset($this->headers[$name]) && is_string($this->headers[$name])) {
            return $this->headers[$name];
        }
        return null;
    }
}
