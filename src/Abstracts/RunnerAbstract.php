<?php

namespace Wtf\Abstracts;

/**
 * Class RunnerAbstract
 * @package Wtf\Abstracts
 */
abstract class RunnerAbstract {
    /**
     * @var string|null
     */
    private $state;

    /**
     * @var int
     */
    private $delayMs = 100;

    abstract public function saveState(): void;

    abstract public function loadState(): void;

    public function setState(?string $state = null): void {
        $this->state = $state;
    }

    public function getState(): ?string {
        return $this->state;
    }

    public function getDelay(): int {
        return $this->delayMs;
    }

    /**
     *
     * @param int $delayMs delay between steps in miliseconds
     */
    public function setDelay(int $delayMs): void {
        $this->delayMs = $delayMs;
    }

    /**
     *
     * @param int $delayS delay between steps in seconds
     */
    public function setDelayInSeconds(int $delayS): void {
        $this->delayMs = $delayS * 1000000;
    }

    public function run(): void {
        if (!$this->beforeRun()) {
            return;
        }

        while (true) {
            if (!$this->beforeStep()) {
                break;
            }

            $result = $this->step();
            if ($result === false) {
                break;
            }

            if (!$this->afterStep()) {
                break;
            }

            usleep($this->delayMs);
        }

        $this->afterRun();
    }

    abstract protected function step(): bool;

    protected function beforeRun(): bool {
        return true;
    }

    protected function afterRun(): void {

    }

    protected function beforeStep(): bool {
        return true;
    }

    protected function afterStep(): bool {
        return true;
    }
}
