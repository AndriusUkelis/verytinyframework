<?php

declare(strict_types = 1);

namespace Wtf\Abstracts;

/**
 * Class CommandAbstract
 * @package Wtf\Abstracts
 */
abstract class CommandAbstract {
    /**
     * @param string $command
     */
    abstract public function execute(string $command);
}
