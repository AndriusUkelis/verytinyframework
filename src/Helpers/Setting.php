<?php

namespace Wtf\Helpers;

class Setting {
    public static function set(string $key, $value) {
        $sqlTpl = 'INSERT INTO `settings` (`key`, `value`) VALUES(\'%s\', \'%s\')
            ON DUPLICATE KEY UPDATE `value` = VALUES(`value`)';
        $sql = sprintf($sqlTpl, Db::escape($key), Db::escape($value));
        Db::query($sql);
    }
    public static function setArray(string $key, array $value) {
        $encodedValue = json_encode($value);
        if ($encodedValue === null) {
            $encodedValue = '[]';
        }

        return self::set($key, $encodedValue);
    }

    public static function getInt(string $key, $default = null): ?int {
        return (int)self::get($key, $default);
    }

    public static function getFloat(string $key, $default = null): ?float {
        return (float)self::get($key, $default);
    }

    public static function getString(string $key, $default = null): ?string {
        return (string)self::get($key, $default);
    }

    public static function getArray(string $key, $default = null): ?array {
        $value = self::get($key, $default);
        $decodedValue = json_decode($value, true);
        if (!is_array($decodedValue)) {
            return $default;
        }

        return $decodedValue;
    }

    public static function get(string $key, $default = null) {
        $sqlTpl = 'SELECT `value` FROM `settings` WHERE `key` = \'%s\'';
        $sql = sprintf($sqlTpl, Db::escape($key));
        $value = Db::fetchValue($sql);
        if ($value === null) {
            return $default;
        }

        return $value;
    }

    public static function delete(string $key) {
        $sqlTpl = 'DELETE FROM `settings` WHERE `key` = \'%s\'';
        $sql = sprintf($sqlTpl, Db::escape($key));
        Db::query($sql);
    }
}
