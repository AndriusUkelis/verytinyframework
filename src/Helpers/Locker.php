<?php

namespace Wtf\Helpers;

use Exception;

class Locker {
    protected $lockFilePath;
    
    /**
     *
     * @var int seconds
     */
    protected $timeout;

    public function __construct(string $lockFilePath, int $timeout = 5 * 60) {
        $this->lockFilePath = $lockFilePath;
        $this->timeout = $timeout;
    }

    public function isLocked(): bool {
        if (file_exists($this->lockFilePath)) {
            $runningSince = (int)file_get_contents($this->lockFilePath);
            if (time() - $runningSince > $this->timeout) {
                $this->unlock();
            } else {
                return true;
            }
        }

        return false;
    }

    public function lock() {
        file_put_contents($this->lockFilePath, time());

        if (!$this->isLocked()) {
            throw new Exception('Cannot lock');
        }
    }

    public function unlock() {
        if (file_exists($this->lockFilePath)) {
            unlink($this->lockFilePath);
        }

        if ($this->isLocked()) {
            throw new Exception('Cannot unlock');
        }
    }
}
