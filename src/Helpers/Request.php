<?php

namespace Wtf\Helpers;

use Exception;

class Request {
    public static function input(string $key, ?string $default = null): ?string {
        $uri = $_SERVER['REQUEST_URI'];

        $urlAndQuery = explode('?', $uri, 2);
        if (count($urlAndQuery) !== 2) {
            return $default;
        }

        $params = [];
        $query = explode('&', $urlAndQuery[1]);
        foreach ($query as $queryItem) {
            $keyValue = explode('=', $queryItem, 2);
            if (count($keyValue) === 2) {
                $params[$keyValue[0]] = $keyValue[1];
            } else {
                $params[$keyValue[0]] = '';
            }
        }

        if (isset($params[$key])) {
            return $params[$key];
        }

        return $default;
    }

    /**
     * @return array|null
     * @throws Exception
     */
    public static function json(): ?array {
        $payload = file_get_contents('php://input');
        if (empty($payload)) {
            return null;
        }

        $decoded = json_decode($payload, true);
        if ($decoded === null) {

            throw new Exception(sprintf(
                'Bad JSON: %s'.PHP_EOL.'Payload: %s',
                json_last_error_msg(),
                $payload
            ));
        }

        return $decoded;
    }
}
