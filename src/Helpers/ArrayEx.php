<?php

namespace Wtf\Helpers;

class ArrayEx {
    const SORT_DIR_ASC = 'asc';
    const SORT_DIR_DESC = 'desc';

    public static function sortByKey(array &$array, string $key, string $sortDir = self::SORT_DIR_ASC) {
        usort($array, function($a, $b) use ($key, $sortDir) {
            if ($sortDir === self::SORT_DIR_DESC) {
                $more = -1;
                $less = 1;
            } else {    // defaults to ASC
                $more = 1;
                $less = -1;
            }

            $aValue = ArrayEx::get($a, $key, 0);
            $bValue = ArrayEx::get($b, $key, 0);
            if ($aValue > $bValue) {
                return $more;
            } elseif ($aValue < $bValue) {
                return $less;
            } else {
                return 0;
            }
        });
    }

    public static function sortByKeyAssoc(array &$array, string $key, string $sortDir = self::SORT_DIR_ASC) {
        uasort($array, function($a, $b) use ($key, $sortDir) {
            if ($sortDir === self::SORT_DIR_DESC) {
                $more = -1;
                $less = 1;
            } else {    // defaults to ASC
                $more = 1;
                $less = -1;
            }

            $aValue = ArrayEx::get($a, $key, 0);
            $bValue = ArrayEx::get($b, $key, 0);
            if ($aValue > $bValue) {
                return $more;
            } elseif ($aValue < $bValue) {
                return $less;
            } else {
                return 0;
            }
        });
    }

    public static function sortByKeys(array &$array, array $keys) {
        usort($array, function($a, $b) use ($keys) {
            foreach ($keys as $key => $direction) {
                if ($direction === self::SORT_DIR_DESC) {
                    $more = -1;
                    $less = 1;
                } else {    // defaults to ASC
                    $more = 1;
                    $less = -1;
                }

                if ($a[$key] > $b[$key]) {
                    return $more;
                } elseif ($a[$key] < $b[$key]) {
                    return $less;
                }
            }
            return 0;
        });
    }

    public static function splitSymbolArrayToStrings(array $symbols, int $limit = 500): array {
        if (empty($symbols)) {
            return [];
        }

        $symbolArray = [];
        $fullString = implode(',', $symbols);

        while (true) {
            $offset = strlen($fullString) - $limit;
            if ($offset <= 0) {
                $symbolArray[] = $fullString;
                break;
            }

            $pos = strrpos($fullString, ',', -$offset);

            if (!$pos) {
                $symbolArray[] = $fullString;
                break;
            }

            $symbolArray[] = substr($fullString, 0, $pos);
            $fullString = substr($fullString, $pos+1);
        }

        return $symbolArray;
    }

    public static function resetKeys(array $array): array {
        return array_values($array);
    }

    public static function inArrayIgnoreCase(string $search, array $array): bool {
        $found = false;
        foreach ($array as $value) {
            if (is_array($value)) {
                $found = self::inArrayIgnoreCase($search, $value);
            } elseif (strtolower($search) == strtolower($value)) {
                $found =  true;
            }

            if ($found) {
                return true;
            }
        }

        return false;
    }

    public static function sumByKey(array $array, string $key): float {
        return array_sum(array_column($array, $key));
    }

    public static function groupByKey(array $array, string $key): array {
        $grouped = [];
        foreach ($array as $item) {
            $keyValue = $item[$key];
            if (!isset($grouped[$keyValue])) {
                $grouped[$keyValue] = [];
            }

            $grouped[$keyValue][] = $item;
        }

        return $grouped;
    }

    public static function groupSinglesByKey(array $array, string $key): array {
        $grouped = [];
        foreach ($array as $item) {
            $keyValue = $item[$key];
            $grouped[$keyValue] = $item;
        }

        return $grouped;
    }

    public static function getFirst(array $array) {
        foreach ($array as $item) {
            return $item;
        }
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param array $array
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function get(array $array, string $key, $default = null) {
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }

        if (strpos($key, '.') === false) {
            return $array[$key] ?? value($default);
        }

        foreach (explode('.', $key) as $segment) {
            if (isset($array) && isset($array[$segment])) {
                $array = $array[$segment];
            } else {
                return $default;
            }
        }

        return $array;
    }

    /**
     * @param array $array
     * @return bool
     */
    public static function isIndexed(array $array): bool {
        return array_keys($array) === range(0, count($array) - 1);
    }
}
