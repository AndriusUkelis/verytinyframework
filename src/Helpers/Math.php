<?php

namespace Wtf\Helpers;

class Math {
    public static function hexToInt(string $hexNumber): int {
        return (int)hexdec(self::unscientify($hexNumber));
    }

    public static function hexToDecimal(string $hexNumber): string {
        if (substr($hexNumber, 0, 2) === '0x') {
            $hexNumber = substr($hexNumber, 2);
        }

        $dec = 0;
        $len = strlen($hexNumber);
        for ($i = 1; $i <= $len; $i++) {
            $dec = bcadd($dec, bcmul(strval(hexdec($hexNumber[$i - 1])), bcpow('16', strval($len - $i))));
        }

        return $dec;
    }

    public static function hex2Str(string $hex, bool $encodeToUtf8 = true): string {
        $str = '';
        for ($i = 0; $i < strlen($hex) - 1; $i += 2) {
            $str .= chr(hexdec($hex[$i].$hex[$i + 1]));
        }

        if (!$encodeToUtf8) {
            return $str;
        }

        return utf8_encode($str);
    }

    public static function bcdechex($dec) {
        $last = bcmod($dec, 16);
        $remain = bcdiv(bcsub($dec, $last), 16);

        if($remain == 0) {
            return dechex($last);
        } else {
            return self::bcdechex($remain).dechex($last);
        }
    }

    public static function intToHex(int $number): string {
        return '0x'.dechex(self::unscientify($number));
    }

    public static function divide($dividend, $divisor, int $precision = 30): string {
        return bcdiv(self::unscientify($dividend), self::unscientify($divisor), $precision);
    }

    public static function multiply($multiplicand, $multiplier, int $precision = 30): string {
        return bcmul(self::unscientify($multiplicand), self::unscientify($multiplier), $precision);
    }

    public static function add($addend1, $addend2, int $precision = 30): string {
        return bcadd(self::unscientify($addend1), self::unscientify($addend2), $precision);
    }

    public static function subtract($minuend, $subtrahend, int $precision = 30): string {
        return bcsub(self::unscientify($minuend), self::unscientify($subtrahend), $precision);
    }

    public static function antiPower($number, int $powerToLowerBy, int $precision = 30): string {
        if ($powerToLowerBy === 0) {
            $power = '1';
        } else {
            $power = '1'.str_repeat('0', $powerToLowerBy);
        }

        return bcdiv(self::unscientify($number), $power, $precision);
    }

    public static function trimTrailingZeros(string $number): string {
        $parts = explode('.', $number);
        if (count($parts) < 2) {
            return $number;
        }

        $parts[1] = rtrim($parts[1], '0');
        if (strlen($parts[1]) === 0) {
            $parts[1] = '0';
        }

        return $parts[0].'.'.$parts[1];
    }

    /**
     * Flat-earther mode: enabled
     */
    public static function unscientify(string $value): string {
        $explodedByExp = explode('E', (string)$value);
        if (count($explodedByExp) !== 2) {
            return (string)$value;
        }
        if (strpos($explodedByExp[1], '-') !== false) {
            return self::antiPower($explodedByExp[0], (string)trim($explodedByExp[1], '-'));
        }

        $secondValue = bcpow('10', $explodedByExp[1]);
        return bcmul($explodedByExp[0], $secondValue);
    }

    /**
     * @param string $sortDir
     * @return callable
     */
    public static function compare($sortDir = ArrayEx::SORT_DIR_ASC): callable {
        if ($sortDir === ArrayEx::SORT_DIR_ASC) {
            return function ($a, $b) {
                return bccomp($a, $b, 30);
            };
        } elseif ($sortDir === ArrayEx::SORT_DIR_DESC) {
            return function ($a, $b) {
                return bccomp($b, $a, 30);
            };
        }
        return function ($a, $b) {
            return 0;
        };
    }
}
