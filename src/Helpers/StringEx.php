<?php

declare(strict_types = 1);

namespace Wtf\Helpers;

use Exception;

class StringEx {

    /**
     * @param string $data
     * @param int $maxChars
     * @param string $splitSymbol
     * @param bool $includeEllipsis
     * @return string
     */
    public static function limitWords(
        string $data,
        int $maxChars,
        string $splitSymbol = ' ',
        bool $includeEllipsis = false
    ): string {
        $words = explode($splitSymbol, $data);
        $wordsCount = count($words);

        if ($wordsCount === 0) {
            return '';
        }
        if ($wordsCount === 1) {
            return $words[0];
        }

        $result = $words[0];

        for ($i = 1; $i < $wordsCount; $i++) {
            if (strlen($result.$splitSymbol.$words[$i]) > $maxChars) {
                break;
            }
            $result .= $splitSymbol.$words[$i];
        }

        return $result.($includeEllipsis ? '...' : '');
    }

    /**
     * @param string $url
     * @return string|null
     */
    public static function rawEncodeUrlAndChangeScheme(string $url): ?string {
        $encodedUrl = self::rawEncodeUrl($url);
        return self::changeUrlScheme($encodedUrl);
    }

    /**
     * @param string $url
     * @return string|null
     */
    public static function rawEncodeUrl(string $url): ?string {
        if (empty($url)) {
            return null;
        }
        $parsedUrl = parse_url($url);
        if (empty($parsedUrl['scheme'])) {
            return null;
        }

        $newUrl = $parsedUrl['scheme'].'://'.$parsedUrl['host'];
        if (isset($parsedUrl['port'])) {
            $newUrl .= ':'.$parsedUrl['port'];
        }
        $pathExploded = explode('/', $parsedUrl['path']);
        foreach ($pathExploded as $key => $pieceOfUrl) {
            if (!empty($pieceOfUrl)) {
                $newUrl .= '/'.rawurlencode($pieceOfUrl);
            }
        }
        return $newUrl;
    }

    /**
     * @param string|null $url
     * @param string|null $scheme
     * @return null|string
     */
    public static function changeUrlScheme(?string $url = null, ?string $scheme = null): ?string {
        if (empty($url)) {
            return null;
        }
        $urlParsed = parse_url($url);
        if (empty($urlParsed) || empty($urlParsed['host'])) {
            return null;
        }

        if (empty($scheme)) {
            $appUrl = env('APP_URL', 'http://localhost');
            $scheme = parse_url($appUrl, PHP_URL_SCHEME);
        }

        $finalUrl = $scheme.'://';

        if (!empty($urlParsed['user'])) {
            $finalUrl .= $urlParsed['user'].':';
        }

        if (!empty($urlParsed['pass'])) {
            $finalUrl .= $urlParsed['pass'].'@';
        } elseif (!empty($urlParsed['user'])) {
            $finalUrl .= '@';
        }

        $finalUrl .= $urlParsed['host'];

        if (!empty($urlParsed['port'])) {
            $finalUrl .= ':'.$urlParsed['port'];
        }

        if (!empty($urlParsed['path'])) {
            $finalUrl .= $urlParsed['path'];
        }

        if (!empty($urlParsed['query'])) {
            $finalUrl .= '?'.$urlParsed['query'];
        }

        if (!empty($urlParsed['fragment'])) {
            $finalUrl .= '#'.$urlParsed['fragment'];
        }

        return $finalUrl;
    }

    /**
     * @param int $length
     * @param string|null $availableChars
     * @return string
     * @throws Exception
     */
    public static function generateAlphanumericString(int $length = 10, string $availableChars = null): string {
        if (empty($availableChars)) {
            $availableChars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        $pieces = [];
        $max = mb_strlen($availableChars, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $availableChars[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    /**
     * @param string $fileName
     * @return bool
     */
    public static function isFileNameEmpty(string $fileName): bool {
        $fileNameOnly = basename($fileName);
        $extension = explode('.', $fileNameOnly);
        return empty($extension[0]) && count($extension) <= 2;
    }

    /**
     * BOTH URLS MUST BE WITH PROTOCOLS
     * @param string $url
     * @param string $validUrl
     * @return bool
     */
    public static function checkIfSameDomain(string $url, string $validUrl): bool {
        $parsedUrl = parse_url($url);
        $parsedValidUrl = parse_url($validUrl);
        if (!self::checkIfHostExist($parsedUrl)
            || !self::checkIfHostExist($parsedValidUrl)
        ) {
            return false;
        }
        $explodedUrl = explode('.', $parsedUrl['host']);
        $explodedValidUrl = explode('.', $parsedValidUrl['host']);
        $reversedValidUrlArray = array_reverse($explodedValidUrl);
        foreach ($reversedValidUrlArray as $pieceOfUrl) {
            if (!in_array($pieceOfUrl, $explodedUrl)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param array $parsedUrl
     * @return bool
     */
    private static function checkIfHostExist(array $parsedUrl): bool {
        if (isset($parsedUrl['host'])
            || !empty($parsedUrl['host'])) {
            return true;
        }
        return false;
    }

    /**
     * @param string $data
     * @return string
     */
    public static function ensureOneSlashBeforeString(string $data): string {
        while (strpos($data, '/') === 0) {
            $data = substr($data, 1);
        }
        return '/'.$data;
    }

    /**
     * Leaves only one specified trailing symbol if it exists at the end of the string
     * @param string $data
     * @param string $trailingSymbol
     * @return string
     */
    public static function leaveOneTrailingSymbolIfExists(string $data, string $trailingSymbol): string {
        // Regex explanation: Skip everything before $trailingSymbol, without including first symbol in the result
        // Then match all the other $trailingSymbol's until end of the string
        // Example regex for "/" symbol: /(?<=\/)\/*$/
        $regex = sprintf('/(?<=%1$s)%1$s*$/', preg_quote($trailingSymbol, '/'));
        return preg_replace($regex, '', $data);
    }

    /**
     * @return string
     */
    public static function getFrontendUrlBase(): string {
        return env('FRONTEND_URL', 'https://localhost');
    }
}
