<?php

namespace Wtf\Helpers;

class Config {
    protected $filepath;

    public function __construct(string $filepath) {
        $this->filepath = $filepath;
    }

    public function exists(): bool {
        return file_exists($this->filepath);
    }

    public function readValues(): array {
        if (!$this->exists()) {
            return [];
        }

        $values = [];
        $lines = file($this->filepath);
        foreach ($lines as $line) {
            $lineTrimmed = trim($line);
            if ($lineTrimmed === '' || $lineTrimmed[0] === '#') {
                continue;
            }

            $keyValue = explode('=', $lineTrimmed, 2);
            if (count($keyValue) < 2) {
                $values[$lineTrimmed] = '';
            }

            $values[$keyValue[0]] = $keyValue[1];
        }

        return $values;
    }
}
